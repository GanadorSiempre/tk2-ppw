from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from quotesgen.models import Quote , QuoteRec

# Create your tests here.
class QuotesGenUnitTest(TestCase):

    def test_model_create(self):
        contoh = Quote.objects.create(quote="if you think you can you can")
        jumlah = Quote.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_model_str(self):
        contoh = Quote.objects.create(quote="yukbisa")
        self.assertEqual("yukbisa", str(contoh))