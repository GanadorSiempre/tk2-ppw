
from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quotesgen', '0002_quotesrec'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='QuotesRec',
            new_name='QuoteRec',
        ),
    ]
