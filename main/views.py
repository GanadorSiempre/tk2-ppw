from django.shortcuts import render
from donation.models import Donate
from report.models import Report

def home(request):
    return render(request, "home.html")

def test(request):
    return render(request, "test.html")

def confirm(request):
    return render(request, "confirm.html")

def contactus(request):
    return render(request, "contactus.html")

def cases(request):
    data = Report.objects.all()
    response = {
        'data' : data
    }
    return render(request, "cases.html", response)

def aboutus(request):
    return render(request, "aboutus.html")

def donate_list(request):
    data = Donate.objects.all()
    response = {
        'data' : data
    }
    return render(request, 'donasi.html', response)