from django import forms
from django.forms import Textarea, TextInput

class reportform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class' : 'form-control'
    }
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Nama ', max_length=160, required=True)
    address = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label = 'Address ', max_length=200, required=True)
    phone = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label = 'Phone ',max_length=15, required=True)
    case = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Judul ', max_length=160, required=True)
    detail = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), label = 'Detail ',max_length=500, required=True)