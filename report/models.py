from django.db import models

# Create your models here.
class Report(models.Model):
    name = models.CharField(max_length=160)
    address = models.CharField(max_length=200)
    phone = models.IntegerField(max_length=15)
    case = models.CharField(max_length=160, default='')
    detail = models.CharField(max_length=500, default='')

