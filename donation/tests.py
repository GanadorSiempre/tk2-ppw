from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve, reverse


from . import views
from donation.models import Donate
from donation.views import post_donation, home
from donation.forms import donateform


class DonationTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get(reverse('donation:home'))
        self.assertEqual(response.status_code, 200)
    
    # def test_root_url_status_200_post(self):
    #     response = Client().get(reverse('donation:post'))
    #     self.assertEqual(response.status_code, 200)

    # def test_post_donation(self):
    #     Client().post('/donation/post/', data={'name':'bambang', 'email':'bambang@mail.com','nominal':'2000'})
    #     donasi = Donate.objects.filter(name='bambang').count()
    #     self.assertEqual(donasi,1)

    def test_template_home(self):
        response = Client().get(reverse('donation:home'))
        self.assertTemplateUsed(response, 'donation.html')

    def test_func_donation(self):
        found = resolve('/donation/post')
        self.assertEqual(found.func, post_donation)

    def test_func_home(self):
        found = resolve('/donation/')
        self.assertEqual(found.func, home)

    def test_model_donasi(self):
        Donate.objects.create(name="waka", email="wakawaka@email.com", nominal="100000")
        donasi = Donate.objects.all().count()
        self.assertEqual(donasi, 1)

    def test_form_donasi(self):
        Client().post('/donation/post',data={'name':'bambang', 'email':'bambang@mail.com','nominal':'2000'})
        donasi = Donate.objects.filter(name='bambang', email='bambang@mail.com',nominal='2000').count()
        self.assertEqual(donasi, 1)

    def test_form_invalid(self):
        form = donateform(data={'name':'', 'email':'','nominal':''})
        self.assertFalse(form.is_valid())
        response = Client().get(reverse('donation:home'))
        self.assertTemplateUsed(response, 'donation.html')
        
