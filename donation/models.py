from django.db import models

# Create your models here.
class Donate(models.Model):
    name = models.CharField(max_length=50)
    email =  models.CharField(max_length=30)
    nominal = models.IntegerField(max_length=30)

