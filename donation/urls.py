from django.urls import path
from . import views

app_name = 'donation'

urlpatterns = [
    path('', views.home, name='home'),
    path('post', views.post_donation, name= 'post'),
]