from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse

from donation.models import Donate
from donation.forms import donateform 
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='perloginan:login')
def home(request):
    form_tambah = donateform()
    data = Donate.objects.all()
    response = {'form_tambah' : form_tambah, 'data' : data}
    return render(request, 'donation.html', response)



def post_donation(request):
    if request.method == 'POST':
        form = donateform(request.POST or None)
        response_data = {}
        if form.is_valid():
            response_data['name'] = request.POST['name']
            response_data['email'] = request.POST['email']
            response_data['nominal'] = request.POST['nominal']

            data_donation = Donate(name=response_data['name'],
                                   email=response_data['email'],
                                   nominal=response_data['nominal'],
                                   )
            data_donation.save()
            return redirect('main:confirm')
