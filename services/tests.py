from django.test import TestCase, Client
from django.urls import resolve, reverse

from . import views
from services.models import Service
from services.views import home, post_service
from services.forms import serviceforms
# Create your tests here.


class ServiceTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get(reverse('services:home'))
        self.assertEqual(response.status_code, 200)
        
    def test_template_home(self):
        response = Client().get(reverse('services:home'))
        self.assertTemplateUsed(response, 'services.html')

    def test_func_service(self):
        found = resolve('/services/post')
        self.assertEqual(found.func, post_service)

    def test_func_home(self):
        found = resolve('/services/')
        self.assertEqual(found.func, home)

    def test_model_services(self):
        Service.objects.create(name='waka', address='rumah waka',phone='0812',detail='di perumahannya')
        service = Service.objects.all().count()
        self.assertEqual(service, 1)

    def test_form_service(self):
        Client().post('/services/post',data={'name':'bambang', 'address':'rumah bambang','phone':'0812','detail':'di kelurahan citayem'})
        service = Service.objects.filter(name='bambang', address='rumah bambang',phone='0812', detail='di kelurahan citayem').count()
        self.assertEqual(service, 1)

    def test_form_invalid(self):
        form = serviceforms(data={'name':'', 'address':'','phone':'','detail':''})
        self.assertFalse(form.is_valid())
        response = Client().get(reverse('services:home'))
        self.assertTemplateUsed(response, 'services.html')
        
