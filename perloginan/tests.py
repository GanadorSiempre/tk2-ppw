from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
#from selenium import webdriver
from .apps import PerloginanConfig
from django.contrib.auth.models import User

from .views import logIn, signUp, logOut
from selenium.webdriver.chrome.options import Options
import os


# Create your tests here.
class PerloginanTestCase(TestCase):
    #LOGIN PAGE TEST
    # def test_root_url_check(self):
    #     response = Client().get(reverse(), follow=True)
    #     self.assertEqual(response.status_code, 200)

    def test_does_login_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/perloginan/login/')
        self.assertEqual(found.func, logIn)

    def test_does_login_views_show_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

    #SIGN UP PAGE TEST
    def test_does_signup_page_exist(self):
        response = Client().get('/perloginan/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_signup(self):
        found = resolve('/perloginan/signup/')
        self.assertEqual(found.func, signUp)

    def test_sign_up_from_page(self):
        user_count = User.objects.all().count()
        data = {
            'username':'sakatagintoki',
            'password1':'shiroyasha07',
            'password2':'shiroyasha07',
            'first_name':'waka',
            'last_name':'waka',
        }
        response = self.client.post('/perloginan/signup/', data = data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), user_count +1)
        
    def test_sign_in_success(self):
        data = {
            'username' : 'gintoki',
            'password' : 'shiroyasha',
        }
        response = self.client.post('/perloginan/login/', data = data)
        # content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 200)

    # def test_create_failed_account(self):
    #     response = Client().post(reverse('perloginan:signup'), {
    #         'username': 'test',
    #         'password1': 'a',
    #         'password2': 'a',
    #         'first_name': 'test',
    #         'last_name': 'test',
    #         }, follow=True)
    #     self.assertIn("failed to create account", response.content.decode('utf-8'))

    # def test_logout_from_page(self):
    #     user_count = User.objects.all().count()
    #     data = {
    #         'username':'sakatagintoki',
    #         'password1':'shiroyasha07',
    #         'password2':'shiroyasha07',
    #         'first_name':'waka',
    #         'last_name':'waka',
    #     }
    #     response = self.client.post('/perloginan/signup/', data = data)
    #     response = self.client.post('/perloginan/logout/')
    #     self.assertEquals(response.status_code, 302)