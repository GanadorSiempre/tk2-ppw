# tkppw2

[![pipeline status](https://gitlab.com/GanadorSiempre/tk2-ppw/badges/master/pipeline.svg)](https://gitlab.com/GanadorSiempre/tk2-ppw/-/commits/master)
[![coverage report](https://gitlab.com/GanadorSiempre/tk2-ppw/badges/master/coverage.svg)](https://gitlab.com/GanadorSiempre/tk2-ppw/-/commits/master)

Kelompok C11 - PPW C
1. Alina Dhifan Ajriya – 1906399474
2. Eyota Wakanda – 1906353832
3. Rio Fernando Alexander – 1906399266
4. Salvian Athallahrif Fadhil – 1906398843

Link herokuapp: http://co-feed2.herokuapp.com
Link gitlab: https://gitlab.com/GanadorSiempre/tk2-ppw


Keberadaan virus corona telah menyita banyak perhatian dunia, tak terkecuali Indonesia. Saat ini, Indonesia merupakan salah satu negara dengan korban terkena virus corona terbanyak di dunia. Akibatnya, banyak kegiatan terkendala dan kebanyakan orang merasa takut, bahkan di rumah sendiri. Oleh karena itu, CoFeed hadir untuk membantu melayani masyarakat selama pandemic. CoFeed menyediakan beberapa fitur aplikasi:
-	Home page sebagai beranda dan pengenalan website
-	Halaman berisikan artikel untuk mengedukasi masyarakat mengenai covid-19
-	Fitur laporkan apabila terdapat masyarakat yang terduga corona
-	Fitur request penyemprotan disinfektan untuk menjaga kebersihan rumah
-	Update kasus untuk mengetahui perkembangan virus corona di Indonesia sekarang

